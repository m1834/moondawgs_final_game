const express = require('express');
const path = require('path');
const http = require('http');
const PORT = process.env.PORT || 3000;
const socketio = require('socket.io');
const app = express();
const server = http.createServer(app);
const io = socketio(server);

// Set path to source folder
app.use(express.static(path.join(__dirname, "source")));

// Wake up server
server.listen(PORT, () => console.log(`Server listening on port ${PORT}`));

// Socket connection request from client
const connections = [null, null];

io.on('connection', socket => {
    // Find available player num
    let playerIndex = -1;
    for(const i in connections) {
        if (connections[i] === null) {
            playerIndex = i;
            break;
        }
    }

    // Tell client which player they are
    socket.emit('player-number', playerIndex);
    console.log(`Player ${playerIndex} has joined`);

    // Boot 3rd player
    if(playerIndex === -1) {
        return;
    }

    connections[playerIndex] = false;

    // Broadcast which player num connected
    socket.broadcast.emit('player-connection', playerIndex);

    // Disconnection
    socket.on('disconnect', () => {
        console.log(`Player ${playerIndex} has left`);
        connections[playerIndex] = null;
        // Broadcast which player num disconnected
        socket.broadcast.emit('player-connection', playerIndex);
    });

    // On player ready up
    socket.on('player-ready', () => {
        socket.broadcast.emit('enemy-ready', playerIndex);
        connections[playerIndex] = true;
    });

    // Check connections
    socket.on('check-players', () => {
        const players = [];
        for (const i in connections) {
            connections[i] === null ? players.push({connected: false, ready: false}) : players.push({connected: true, ready: connections[i]});
        }
        socket.emit('check-players', players);
    });

    // Fire received
    socket.on('fire', id => {
        console.log(`Shot fired from ${playerIndex}`, id);

        // Broadcast the move
        socket.broadcast.emit('fire', id);
    });

    // On fire reply
    socket.on('fire-reply', square => {
        console.log(square);

        // Broadcast reply to other player
        socket.broadcast.emit('fire-reply', square);
    });

    // Connection timeout (10 min limit)
    setTimeout(() => {
        connections[playerIndex] = null;
        socket.emit('timeout');
        socket.disconnect();
    }, 600000);
});